﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DeadZone : MonoBehaviour {

    public Text scorePlayerText;
    public Text scoreEnemyText;

    int scorePlayerQuantity;
    int scoreEnemyQuantity;

    public SceneChanger sceneChanger;

    public AudioSource pointAudio;
    private void OnTriggerEnter2D(Collider2D ball){
        if (this.tag == "Izquierdo")
        {
            scoreEnemyQuantity++;
            this.UpdateScoreLabel(scoreEnemyText, scoreEnemyQuantity);
        } else if(this.CompareTag("Derecho")){
            scorePlayerQuantity++;
            this.UpdateScoreLabel(scorePlayerText, scorePlayerQuantity);
        }
        
        ball.GetComponent<BallBehaviour>().gameStarted = false;
        this.CheckScore();
        pointAudio.Play();
    }

    void UpdateScoreLabel(Text label, int score){
        label.text = score.ToString();
    }

    void CheckScore(){
        if(this.scorePlayerQuantity >= 3){
            sceneChanger.ChangeSceneTo("WinScene");
        } else if (scoreEnemyQuantity >= 3){
            sceneChanger.ChangeSceneTo("LoseScene");
        }
    }
}
